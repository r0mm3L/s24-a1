db.inventory.insertMany([
	{
		name: "Captain America Shield",
		price: 50000,
		qty: 17,
		company: "Hydra and Co"
	},
	{
		name: "Mjolnir",
		price: 75000,
		qty: 24,
		company: "Asgard Production"
	},
	{
		name: "Iron Man Suit",
		price: 25400,
		qty: 25,
		company: "Stark Industries"
	},
	{
		name: "Eye of Agamotto",
		price: 28000,
		qty: 51,
		company: "Sanctum Company"
	},
	{
		name: "Iron Spider Suit",
		price: 30000,
		qty: 24,
		company: "Stark Industries"
	}
])

// Query Operators
	// allows us to be more flexible when querying in MongoDB, we can opt to find, update and delete documents based on some condition instead of just specific criterias.

// Comparison Query Operators
	// $gt and $gte
	// Syntax:
		db.collections.find({field: {$gt: value}})
		db.collections.find({field: {$gte: value}})

	// $gt- greater than, allows us to find values that are greater than the given value.
	// $gte- greater than or equal, allows us to find values that are greater than or equal the given value.

	db.inventory.find({price: {$gte: 75000}})

	// $lt and $lte
	// Syntax:
		db.collections.find({field: {$lt: value}})
		db.collections.find({field: {$lte: value}})

	// $lt- less than, allows us to find values that are less than the given value.
	// $lte- less than or equal, allows us to find values that are less than or equal the given value.

	db.inventory.find({qty: {$lt: 20}})

	// $ne
	// syntax:

	db.collections.find({field: {$ne: value}})

	// $ne - not equal, returns a document that values are not equal  to the given value

	db.inventory.find({qty: {$ne: 10}})

	// $in
	// syntax:
	db.collections.find({field: {$in: value}})


	// $in - allows us to find documents that satisfy either of the specified values

	db.inventory.find({price: {$in: [25400, 30000]}})






	/*

	activity

db.inventory.find({price: {$gte: 50000}})

	*/

	db.inventory.find({price: {$gte: 50000}})

	db.inventory.find({qty: {$in: [24, 16]}})




	// UPDATE AND DELETE
			// 2 arguments namely: query criteria, update

			db.inventory.updateMany({price: {$lt: 28000}}, {$set: {qty: 17}})


// activity

db.inventory.updateMany({price: {$lt: 28000}}, {$set: {qty: 17}})

// logical operators
	// $and
	// syntax:
		db.collection.find({$and: [{criteria}, {criteria2}]})

	// $and - allows us to return document/s that satisfies all givenn conditions.

	db.inventory.find({$and: 

		[

		{price: {$gte: 50000}}, 
		{qty: 17}

		]
	})


	// $or
	// syntax:

		db.collections.find({$or: 

			[

			{criteria}, 
			{criteria2}
			]

		})

	db.collections.find({$or: [{criteria}, {criteria2}]})


		// $or - allows us to return document/s that satisfies either of the given conditions.

		db.inventory.find({$or:
			[	
			{qty: {$lt: 24}},
			{isActive: false}
			]
		}


		db.inventory.find({$or: [{qty: {$lt: 24}},{isActive: false}], {$set: {qty: 17}}) // wrong answer activity

		// activity

// answer

db.inventory.updateMany(
{
	$or:
	[
	{ qty: {$lte: 24} },
	{price: {$lte: 30000}}
	]
},
{
	$set:
	{isActive: true}


})





// evaluation query operator
// $regex
	// syntax
		{field: {$regex: /pattern/}}
			// case sensitive query
		{field: {$regex: /pattern/, $options: '$optionValue'}}
		// case insensitive query


		db.inventory.find(
		{
			name: {$regex: 'S'}
			
		}
		)


		db.inventory.find(
		{
			name: {$regex: 'S', $options: '$i'}
			
		}
		)

		// activity

		db.inventory.find(
		{
			$and:
			[
			{name: {$regex: 'i', $options: '$i'}},
			{price: {$gt: 70000}}
			]
			
		}
		)


////////  field projection

		// allows us to hide/show properties of a returned document/s after a query. 
		//when dealing with complex data structures, there might be instances when fields are not useful for the query
		// that we are trying to accomplish

		// incusion and exclusion
				// syntax field 1:
				db.collections.find({criteria}, {field: 1})
					// field: 1 - allows us to include/add specific fields only when retrieving documents.
					// the value provided is 1 to donate that the field is being included.

					db.inventory.find({}, {name:1})

						db.inventory.find({}, {name:1, _id:0})

						db.inventory.find({}, {_id:0, name:1, price:1})

				// syntax field 0:
				db.collections.find({criteria}, {field:0})
					// field: 0 - allows us to exclude the specific field.

					db.inventory.find({}, {qty:0})

					db.inventory.find({}, {_id:0, name:0, price:0})

		// reminder: when using field projection, field inclusion and exclusion may not be used at the same itme. excluding the "_id" field.


		db.inventory.find({company: {$regex: 'A'}}, {name: 1, company:1, _id:0})





		// ACTIVITY













db.collection.find(
	{
		$and: 
		[
		{
			name: {$regex: 'A'}
		}, 
		{
			price: {$lte: 30000}
		}
		]
	}, 
	{
		name:1, price:1, _id:0
	}
	)






